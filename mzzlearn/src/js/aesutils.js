/**
 * 工具类
 */

// import Vue from 'vue'
import CryptoJS from 'crypto-js'

export default { // 加密
  getKey (key) {
    let realKey = CryptoJS.SHA1(key)
    realKey = CryptoJS.SHA1(realKey).toString().substring(0, 32) // 真正的key
    return realKey
  },
  encrypt (data, key) {
    // var realKey = getKey(key);
    let realKey = CryptoJS.SHA1(key)
    realKey = CryptoJS.SHA1(realKey).toString().substring(0, 32)
    let encrypt = CryptoJS.AES.encrypt(data, CryptoJS.enc.Hex.parse(realKey), {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    })
    return encrypt.ciphertext.toString(CryptoJS.enc.Base64)
  },
  decrypt (data, key) {
    let realKey = CryptoJS.SHA1(key)
    realKey = CryptoJS.SHA1(realKey).toString().substring(0, 32)
    // var realKey = getKey(key);
    let decrypt = CryptoJS.AES.decrypt({
      ciphertext: CryptoJS.enc.Base64.parse(data)
    }, CryptoJS.enc.Hex.parse(realKey), {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    })
    return decrypt.toString(CryptoJS.enc.Utf8)
  }

}
