# VueLearn

## 介绍

学习Vue

### [2018-09]

Vue:

```shell
# Vue Node 启动命令
npm install 
npm run dev:test

# 本地构建
npm run build:dev

```

### [2018-11-20]

```shell

特别注意：
1.package.json 文件中设置 "build:dev" ！！！！
2.调整.evn.devEnv配置中的VUE_APP_URL

```	


### [2019-06-18] my-project 项目

[自动化构建工具Vue-CLI](https://cli.vuejs.org/zh/)

```shell
# 1. install vue cli

npm install -g @vue/cli

# 2. create project

vue create my-project

# 3. 快速集成插件到具体项目  （区别：npm install router)

vue add router

# 4. 安装轻量级http服务
npm install -g serve

# 5.调试单个文件-->快速查错 （必须安装http服务)
vue serve Helloworld.vue

# 6.前后端数据交互   axios
# 6.1 node 模拟后端服务  ---> node nodeServer.js
# 6.2 在Helloworld.vue 中添加接口调用

```


参考文档：

[Vue 官网](https://cn.vuejs.org/v2/guide/)

[Vue CLI](https://cli.vuejs.org/zh/)

[菜鸟教程-Vue](http://www.runoob.com/vue2/vue-tutorial.html)

[Vue调试工具](https://www.cnblogs.com/yuqing6/p/7440549.html)