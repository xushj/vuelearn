//vue.config.js  可选配置  --> package.json 分析 -->vue-cli-service --> node_modules --> .... 
//如果有改文件，则会自动加载
//具体配置参数参考：https://cli.vuejs.org/zh/config/#vue-config-js
module.exports={

    publicPath:'./',
    outputDir:'dist',//可以直接打开index.html文件进行访问
    devServer:{
        port: 5000,
        proxy: {   //代理：跨域设置
            '/api': {
              target: "http://localhost:3000", //process.env.VUE_APP_URL, //代理URL
              ws: true,
              changeOrigin: true,
              pathRewrite: {'^/api' : ''},
            }
        }
    }
}