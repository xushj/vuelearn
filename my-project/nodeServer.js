/* eslint-disable no-console */

/**
 * 1. 启动命令（当前目录下）：node  nodeServer.js
 * 2. 页面访问： http://localhost:3000/student/info
 * 3. 前端需要安装插件： vue add axios
 */
//Node.js 提供服务   ---> 测试axios 前后端数据交互 
var express = require('express'); //引入express框架

var bodyParser = require('body-parser');//body解析

var app =new express();

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());


app.get("/student/info",function(req,res){

  res.json({
    message:"sucesss",
    data:{"info":"Her English and Chinese is very good !"}
  })
})


app.listen(3000,()=>{
  console.log("Server started responding on port 3000.");
})